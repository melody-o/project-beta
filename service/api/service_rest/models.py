from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    # class Meta:
    #     app_label = 'service_rest'


class CustomerVO(models.Model):
    first_name = models.CharField(max_length=40, null=True)
    last_name = models.CharField(max_length=40, null=True)
    # class Meta:
    #     app_label = 'service_rest'

class Technician(models.Model):
    first_name = models.CharField(max_length=40, null=True)
    last_name = models.CharField(max_length=40, null=True)
    employee_id = models.CharField(max_length=40, null=True)

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=255)
    status = models.CharField(max_length=255)
    vin = models.CharField(max_length=17)
    customer = models.ForeignKey(CustomerVO,
                                 on_delete=models.CASCADE,
                                 related_name='appointments')
    technician = models.ForeignKey(AutomobileVO,
                                   on_delete=models.CASCADE,
                                   related_name='appointments')
