import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadData() {
  const automobileResponse = await fetch('http://localhost:8100/api/automobiles/');
  const manufacturerResponse = await fetch('http://localhost:8100/api/manufacturers/');
  const vehicleModelResponse = await fetch('http://localhost:8100/api/models/');
  const customerResponse = await fetch('http://localhost:8090/api/customers/');
  const salesPeopleResponse = await fetch('http://localhost:8090/api/salespeople/');
  const salesResponse = await fetch('http://localhost:8090/api/sales/');


try {
  if (
  automobileResponse.ok && manufacturerResponse.ok && vehicleModelResponse.ok && customerResponse.ok && salesPeopleResponse.ok && salesResponse.ok
  ){
    const automobile = await automobileResponse.json();
    const manufacturer = await manufacturerResponse.json();
    const vehicleModel = await vehicleModelResponse.json();
    const customer = await customerResponse.json();
    const salesPeople = await salesPeopleResponse.json();
    const sale = await salesResponse.json();


    root.render(
      <React.StrictMode>
          <App
              automobiles={automobile.automobiles}
              manufacturers={manufacturer.manufacturers}
              vehicleModels={vehicleModel.vehicleModels}
              customers={customer.customers}
              salespeople={salesPeople.salespeople}
              sales={sale.sales}
            />
      </React.StrictMode>
      );
    }
  } catch(e) {
  console.error(e);

  }
}
loadData();
