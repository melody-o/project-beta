import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';

function SalesPersonForm() {

    const [name, setName] = useState('');
    const [employee_id, setEmployeeNumber] = useState('');
    const navigate = useNavigate();



    const handleChangeName = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleChangeEmployeeNumber = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
    }



    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.name = name;
        data.employee_id = employee_id;

        const salesPersonUrl = "http://localhost:8090/api/salespeople/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const salespeopleResponse = await fetch(salesPersonUrl, fetchConfig);

        if(salespeopleResponse.ok) {
            const addSalesPerson = await salespeopleResponse.json();


            setName('');
            setEmployeeNumber('');
            navigate('/sales')

        }
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an sales person!</h1>
                    <form onSubmit={handleSubmit} id="add-auto-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeName} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeEmployeeNumber} value={employee_id} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>

    )

}
export default SalesPersonForm;
