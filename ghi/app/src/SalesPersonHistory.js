import React, { useEffect, useState } from "react";


function SalesPersonHistory() {
    const [salesperson, setSalesPerson] = useState();
    const [salespeople, setSalesPeople] = useState([]);
    const [sales, setSales] = useState([]);



    const handleChangeSalesPerson = (event) => {
        const value = event.target.value;
        setSalesPerson(parseInt(value));
    }

    const fetchData = async () => {
        const salesUrl = "http://localhost:8090/api/sales";

        const salesResponse = await fetch(salesUrl);
        if (salesResponse.ok) {
            const data = await salesResponse.json();
            setSales(data.sales)
        }

        const salesPeopleUrl = "http://localhost:8090/api/salespeople";
        const salesPeopleResponse = await fetch(salesPeopleUrl);
        if (salesPeopleResponse.ok) {
            const data = await salesPeopleResponse.json();
            setSalesPeople(data.salesperson)
        }
        console.log(salesResponse)
    }


    useEffect(() => {
        fetchData();
        }, []);



    return(
        <>
        <select onChange={handleChangeSalesPerson} value={salesperson} placeholder="salesperson" name="salesperson" id="salesperson" className="form-select">
                                <option value="">Choose a Salesperson</option>
                                {salespeople.map(person => {
                                    return(
                                        <option key={person.employee_id} value={person.employee_id}>
                                                {person.name}
                                        </option>
                                    )
                                })}
                            </select>
        <table className="table table-image">
                <thead>
                    <tr>
                        <th scope="col">Sales Person</th>
                        <th scope="col">Employee Number</th>
                        <th scope="col">Customer</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.filter(sale => sale.salesperson.employee_id == salesperson).map(sale => {
                        return (
                            <tr key={sale.automobile.vin}>
                                <td>{ sale.salesperson.name }</td>
                                <td>{ sale.salesperson.employee_id }</td>
                                <td>{ sale.customer.name }</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>{ sale.price }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}
export default SalesPersonHistory
