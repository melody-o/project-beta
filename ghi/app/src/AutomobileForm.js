import React, { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';

function AutomobileForm() {

    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [models, setModels] = useState([]);
    const [model_id, setModelID] = useState('');
    const navigate = useNavigate();


    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleChangeYear = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const handleChangeVin = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleChangeModelID = (event) => {
        const value = event.target.value;
        setModelID(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model_id;

        const automobileUrl = "http://localhost:8100/api/automobiles";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const automobileResponse = await fetch(automobileUrl, fetchConfig);

        if(automobileResponse.ok) {
            const addAutomobile = await automobileResponse.json();


            setColor('');
            setYear('');
            setVin('');
            setModelID('');

            navigate('/automobiles')

        }
    }

    const fetchData = async () => {

        const modelUrl = "http://localhost:8100/api/models/"

        const response = await fetch(modelUrl);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }

    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an automobile to inventory</h1>
                    <form onSubmit={handleSubmit} id="add-auto-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeYear} value={year} placeholder="year" required type="text" name="year" id="year" className="form-control" />
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeVin} value={vin} placeholder="vin" type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleChangeModelID} value={model_id} placeholder="model_id" name="model_id" id="model_id" className="form-select">
                                <option value="">Choose a Model</option>
                                {models.map(model => {
                                    return(
                                        <option key={model.id} value={model.id}>
                                            {model.name} {model.manufacturer.name}
                                        </option>
                                    )
                                })}
                            </select>
                            <label htmlFor="model">Model</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>

    )

}
export default AutomobileForm;
