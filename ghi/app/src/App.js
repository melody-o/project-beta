import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import VehicleModelForm from './VehicleModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from  './AutomobileForm';
import VehicleModelsList from './VehicleModelsList';
import AppointmentList from './AppointmentsList';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import SaleForm from './SaleForm';
import SaleList from './SaleList';
import SalesPersonForm from './SalesPersonForm';
import SalesPersonHistory from './SalesPersonHistory';
import CustomerForm from './CustomerForm';



function App(props) {

import VehicleModelsList from './VehicleModelList';
import AutomobileForm from './AutomobileForm';



import AutomobileList from './AutomobileList';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={< ManufacturerList manufacturers={props.manufacturers} />} />
            <Route path="new" element={<ManufacturerForm/>} />
          </Route>
          <Route path="automobiles">
              <Route path="" element={<AutomobileList automobiles={props.automobiles}/>}/>
              <Route path="new" element={<AutomobileForm models={props.vehicleModels} />}/>
            </Route>
          <Route path="models">
              <Route index element={< VehicleModelsList models={props.models} />} />
              <Route path="new" element={<VehicleModelForm/>} />
          </Route>
          <Route path="customers">
          <Route path="new" element={<CustomerForm/>} />
          </Route>
          <Route path="sales">
              <Route index element={< SaleList models={props.sales} />} />
              <Route path="new" element={<SaleForm/>} />
          </Route>
          <Route path="salespeople">
              <Route index element={< SalesPersonHistory models={props.sales} />} />
              <Route path="new" element={<SalesPersonForm/>} />
          </Route>
          <Route path="technician">
            <Route index element={< TechnicianForm technician={props.technician} /> } />
            <Route path="new" element={TechnicianForm} />
          </Route>
          <Route path="appointments">
            <Route index element={< AppointmentList appointment={props.appointment} /> } />
            <Route path="new" element={<AppointmentForm/>} />
          </Route>
          <Route path="service">
            <Route index element={<AppointmentForm/>} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={< VehicleModelsList models={props.models} />} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={< AutomobileList models={props.automobiles} />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
