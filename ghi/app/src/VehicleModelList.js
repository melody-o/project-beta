import React from 'react';
import { useState, useEffect } from 'react';

function VehicleModelsList(props) {
    const [vehicles, setVehicles] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();

            setVehicles(data.models)
        }
    }
    useEffect(() => {
        fetchData();
        }, []);


    return (

        <>
        <p></p>
        <a href="http://localhost:3000/models/new/"><button type="button" className="btn btn-success">Add Vehicle Model</button></a>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Picture</th>
                        <th scope="col">Model</th>
                        <th scope="col">Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {vehicles.map(vehicle => {
                        return (
                            <tr key={ vehicle.href }>
                                <td className="w-25"><img src={vehicle.picture_url} className="img-thumbnail img"></img></td>
                                <td>{ vehicle.name }</td>
                                <td>{ vehicle.manufacturer.name }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}
export default VehicleModelsList;
