import React from 'react';
import { useEffect, useState } from "react";

function AutomobileList(props) {
    const [automobiles, setAutomobiles] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setAutomobiles(data.automobiles)
        }
    }
    useEffect(() => {
        fetchData();
        }, []);


    return (
        <>
        <a href="http://localhost:3000/automobiles/new/"><button type="button" className="btn btn-success">Add a Automobile</button></a>

        <table className="table table-image">
                <thead>
                    <tr>
                        <th scope="col">Image</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Color</th>
                        <th scope="col">Year</th>
                        <th scope="col">Model</th>
                        <th scope="col">Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles?.map(automobile => {
                        return (
                            <tr key={automobile.vin}>
                                <td className="w-25"><img src={automobile.model.picture_url} className="img-thumbnail img"></img></td>
                                <td>{ automobile.vin }</td>
                                <td>{ automobile.color }</td>
                                <td>{ automobile.year }</td>
                                <td>{ automobile.model.name }</td>
                                <td>{ automobile.model.manufacturer.name }</td>
                                <td>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
};

export default AutomobileList;
